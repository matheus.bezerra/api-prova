carros = [
    {"id":1,
      "modelo":"208",
      "ano":"2012",
      "foto":"https://image.webmotors.com.br/_fotos/AnuncioUsados/gigante/2022/202210/20221028/PEUGEOT-208-1.5-ALLURE-8V-FLEX-4P-MANUAL-wmimagem13101050729.jpg",
      "fabricante":"Peugeot",
      "preco": "30.000"
    },
    {"id":2,
      "modelo":"Toro",
      "ano":"2018",
      "foto":"https://cdn.motor1.com/images/mgl/9xWnl/s1/fiat-toro-freedom-18-2018.jpg",
      "fabricante":"Fiat",
      "preco": "95.000"
    },
    {"id":3,
      "modelo":"Onix",
      "ano":"2020",
      "foto":"https://s2.glbimg.com/Gvr5vyLk0MEnzqIJtFQVOaCbT_8=/0x0:6554x4374/1008x0/smart/filters:strip_icc()/i.s3.glbimg.com/v1/AUTH_59edd422c0c84a879bd37670ae4f538a/internal_photos/bs/2019/u/b/gj50PcTpabRFFE8jRaJA/26-novo-onix-2-.jpg",
      "fabricante":"Chevrolet",
      "preco": "45.000"
    },
    {"id":4,
      "modelo":"Palio",
      "ano":"2015",
      "foto":"https://s2.glbimg.com/HJkGA-GJM6CDS_fjf3XGzRRdESA=/0x0:620x400/600x0/smart/filters:gifv():strip_icc()/i.s3.glbimg.com/v1/AUTH_cf9d035bf26b4646b105bd958f32089d/internal_photos/bs/2020/X/a/mObToPRTmoZCbQzgvAOA/2015-01-06-fiat-palio-fire.jpg",
      "fabricante":"Fiat",
      "preco": "17.000"
    },
    {"id":5,
      "modelo":"Gol",
      "ano":"2005",
      "foto":"scadaveiculos.com.br/carros/e7791a4386bbdf4dfa87027e8dbde654-thumbjpeg-volkswagen-gol-8691881-1000-750-70.jpg",
      "fabricante":"Volkswagen",
      "preco": "15.000"
    },
    {"id":6,
      "modelo":"Corsa",
      "ano":"2012",
      "foto":"https://img2.icarros.com/dbimg/galeriaimgmodelo/2/10651_1.jpg",
      "fabricante":"Chevrolet",
      "preco": "14.000"
    }
]

