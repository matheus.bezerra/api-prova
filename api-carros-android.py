from flask import Flask, jsonify, request
import json
import urllib.request
import random
import carros

app = Flask(__name__)

dic = carros.carros   

@app.route("/carros", methods=['GET'])
def get():
    return jsonify(dic)


@app.route("/carros", methods=['POST'])
def post():
    global dic
    try:
        content = request.get_json()
        
        # gerar id
        ids = [carro["id"] for carro in dic]
        if ids:
            nid = max(ids) + 1
        else:
            nid = 1
        content["id"] = nid
        
        dic.append(content)
        return jsonify({"status":"OK", "msg":"carro adicionado com sucesso"})
    except Exception as ex:
        return jsonify({"status":"ERRO", "msg":str(ex)})

@app.route("/carros/<int:id>", methods=['DELETE'])
def delete(id):
    global dic
    try:
        dic = [carro for carro in dic if carro["id"] != id]
        return jsonify({"status":"OK", "msg":"carro removido com sucesso"})
    except Exception as ex:
        return jsonify({"status":"ERRO", "msg":str(ex)})


if __name__ == "__main__":
    app.run(host='0.0.0.0')
